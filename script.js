const items = document.getElementsByTagName('li');
let i = 0;
const images = Object.values(document.getElementsByTagName('img'));
const img = images.length ? images[0] : null;
const defaultImage = img ? img.src : null;

const setImage = (e) => {
  if (!img) return;
  if (!defaultImage) return;
  if (e?.dataset.image) {
    img.src = e.dataset.image;
  } else {
    img.src = defaultImage;
  }
}

const setActive = (x) => {
  i = x;
  Object.values(items).forEach((e, index) => {
    if (index + 1 < x) {
      e.className = 'old';
    } else if (index + 1 > x) {
      e.className = 'hidden';
    } else {
      e.className = 'current';
      setImage(e);
    }
  });
  if (i === 0) setImage(null);
}

const handleImage = (el) => {

}

const fadeOut = () => {
  Object.values(document.getElementsByTagName('body')).forEach(e => {
    e.className = 'fade-out';
  });
}

const nextPage = () => {
  const button = document.getElementById('next');
  if (!button) return;
  fadeOut();
  setTimeout(() => button.click(), 100);
}

const nextPoint = () => {
  if (i >= items.length) {
    nextPage();
    return;
  }
  setActive(i + 1);
}

const prevPage = () => {
  const button = document.getElementById('prev');
  if (!button) return;
  fadeOut();
  setTimeout(() => button.click(), 1);
}

const prevPoint = () => {
  if (i <= 0) {
    prevPage();
    return;
  }
  setActive(i - 1);
}

document.addEventListener('click', nextPoint);
document.addEventListener('contextmenu', (e) => {
  e.preventDefault();
  prevPoint();
});

document.addEventListener('keydown', (e) => {
  console.log(e.key);
  if ([
    'F12', 'F11', 'F5',
    'Alt', 'Shift', 'OS',
    'AltGraph', 'Escape', 'Control',
  ].includes(e.key)) return;
  else if (e.key === 'ArrowLeft') prevPage();
  else if (e.key === 'ArrowRight') nextPage();
  else if (e.key === 'ArrowUp') prevPoint();
  else nextPoint();
});
