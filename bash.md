# Slides

 1. Story:              2 years ago * LAMP stack * server management
   * I had no choice, but why would you use commandline?
 2. Intro:              Hands on keyboard * simple interface * scripting
   * Get into the meat of it
 3. The temp dir:       /tmp/ * Deleted on restart * Useful for testing
 4. Directing output:   > * >>
 5. Searching files:    find: search for files * grep: search in files
 6. Combining commands: | * && * ||
 7. Previous commands:  history * ctrl+r * history | grep
 8. Going back:         cd - * git switch -
 9. Programmability:    alias * scripts * type
11. Fun commands:       bat * ag
12. Help:               -h, --help flag * help * tldr
13. Action:             Find me after

I did partitions, and permissions, and process management

alias is pretty much keeping something copied and pressing ctrl+v before each
command

pipe history into grep and use !# to run command

# Images

1. Scorchsoft logo
2. ls vs file manager
3. -
4. history printout, ctrl+r on new slide, video of cd - and git switch -
5. pipe into grep, `[[ -f file ]] && cat file | grep str || echo "Not found"`
6. Show different behaviour around cd etc in video
7. type ls, an alias, a function, and a script
8. show both inside react project, maybe vs grep
9. show git help and tldr for ln 
10. Picture of me

