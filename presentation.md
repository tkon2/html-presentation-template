# Slides

 1. Why have slides:   reminders * illustrations * Emphesising points
 2. What I look for:   dark background * points in focus * uniformity
 3. More:              few points * short points * more slides * pictures
 4. Not just me:       David JP Phillips * https://youtu.be/Iwpi1Lm6dFo
 5. How to powerpoint: animate points * animate between slides * master slides
 6. Powerpoint bloat:  does a lot * hard to navigate * custom file format
 7. My solution:       HTML * JS * CSS
 8. Why:               known technology * plain text files * git
 9. Benefits:          change styles easily * write custom slides * works anywhere
10. How:               JS event listener * class names * transitions
11. What about you:    dumb problem * dumb solution * ...profit?
12. Take action:       Make your own dumb solution!


# Images
 1. -
 2. -
 4. Picture of David Phillips done
 5. Screenshots of powerpoint 
 6. More screenshots
 7. Code screenshots
 8. Linus fuck you on 3
 9. -
10. Detailed code screenshots
11. -
12. -


